//
//  USConfig.swift
//  ATPromiseKit
//
//  Created by Dejan on 01/10/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

protocol APIConfigurable {
    var APIKey: String { get }
    var Host: String { get }
    var Endpoint: String { get }
}

struct USConfig: APIConfigurable {
    let APIKey = "YOUR_API_KEY_HERE"
    let Host = "https://api.unsplash.com"
    let Endpoint = "/photos/random"
}
